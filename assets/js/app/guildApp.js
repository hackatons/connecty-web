var legislatorApp = angular.module('guild', ['ngMaterial', 'ui.router' , 'ngSails', 'luegg.directives', 'pascalprecht.translate']);

legislatorApp.config(function ($stateProvider, $urlRouterProvider, $locationProvider, $mdThemingProvider, $translateProvider) {

  $mdThemingProvider.theme('default')
    .primaryPalette('lime')
    .accentPalette('grey')
    .warnPalette('blue-grey');

  $locationProvider.html5Mode({
    enabled: true,
    requireBase: false
  });

  $stateProvider
    .state('login', {
      url: '/login',
      templateUrl: '/templates/login.html'
    });

  $stateProvider
    .state('index', {
      url: '/',
      templateUrl: '/templates/index.html'
    });

  $stateProvider
    .state('download', {
      url: '/download',
      templateUrl: '/templates/download.html'
    });
   $stateProvider
    .state('testpage', {
      url: '/testpage',
      templateUrl: '/templates/testpage.html'
      });

  $urlRouterProvider.otherwise('/');

  $translateProvider.useUrlLoader("/api/language");
  $translateProvider.preferredLanguage("es");

});
