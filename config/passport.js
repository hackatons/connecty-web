/**
 * Passport configuration
 *
 * This is the configuration for your Passport.js setup and where you
 * define the authentication strategies you want your application to employ.
 *
 * I have tested the service with all of the providers listed below - if you
 * come across a provider that for some reason doesn't work, feel free to open
 * an issue on GitHub.
 *
 * Also, authentication scopes can be set through the `scope` property.
 *
 * For more information on the available providers, check out:
 * http://passportjs.org/guide/providers/
 */

module.exports.passport = {
  local: {
    strategy: require('passport-local').Strategy
  },
  facebook: {
    name: 'Facebook',
    protocol: 'oauth2',
    strategy: require('passport-facebook').Strategy,
    options: {
      clientID: '1057044084315199',
      clientSecret: '0591bd6778dae328d6950fbe70092736',
      scope: ['email'] /* email is necessary for login behavior */
    }
  },

  google: {
    scope: ['email'],
    name: 'Google',
    protocol: 'oauth2',
    strategy: require('passport-google-oauth').OAuth2Strategy,
    options: {
      clientID: '588248919089-cv3ve69c4eqr0kr9b6mca7v6tsl09615.apps.googleusercontent.com',
      clientSecret: 'r-P_J-bBkUrmRcwwe0SLHF8K'
    }
  }
};
