/**
 * GuildController
 *
 * @description :: Server-side logic for managing guilds
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	


  /**
   * `GuildController.project()`
   */
  project: function (req, res) {
    return res.json({
      todo: 'project() is not implemented yet!'
    });
  },


  /**
   * `GuildController.board()`
   */
  board: function (req, res) {
    return res.json({
      todo: 'board() is not implemented yet!'
    });
  },


  /**
   * `GuildController.team()`
   */
  team: function (req, res) {
    return res.json({
      todo: 'team() is not implemented yet!'
    });
  },


  /**
   * `GuildController.chat()`
   */
  chat: function (req, res) {
    return res.json({
      todo: 'chat() is not implemented yet!'
    });
  }
};

