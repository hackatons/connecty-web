/**
 * ProjectController
 *
 * @description :: Server-side logic for managing projects
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	


  /**
   * `ProjectController.board()`
   */
  board: function (req, res) {
    return res.json({
      todo: 'board() is not implemented yet!'
    });
  },


  /**
   * `ProjectController.team()`
   */
  team: function (req, res) {
    return res.json({
      todo: 'team() is not implemented yet!'
    });
  },


  /**
   * `ProjectController.chat()`
   */
  chat: function (req, res) {
    return res.json({
      todo: 'chat() is not implemented yet!'
    });
  }
};

