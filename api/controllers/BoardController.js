/**
 * BoardController
 *
 * @description :: Server-side logic for managing boards
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	


  /**
   * `BoardController.team()`
   */
  team: function (req, res) {
    return res.json({
      todo: 'team() is not implemented yet!'
    });
  },


  /**
   * `BoardController.chat()`
   */
  chat: function (req, res) {
    return res.json({
      todo: 'chat() is not implemented yet!'
    });
  }
};

